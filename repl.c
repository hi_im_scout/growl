/*
  
  @author hi_im_scout
*/

#include <stdio.h>
#include <stdlib.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <stdarg.h>
#include "common.h"
#include "printer.h"
#include "reader.h"
#include "repl.h"

Object *nil;
Object *true;
Object *false;
Environment **global_env;

int main(void) {
  global_env = malloc(sizeof(Environment*));
  Environment *env = init_environment();
  global_env = &env;

  puts("\n[VERSION " VERSION "]\n");
  
  while(1) {
    print(eval(read_lisp(), *global_env));
    printf("\n");
  }
  
  return 0;
}

Environment *new_Environment(void) {
  Environment *e = calloc(sizeof(Environment), 1);
  return e;
}

char *pr_str(Object *o) {
  char *int_string;
  char *float_string;
  char *list_string;
  LLNode *current;
  
  if(o == NULL) {
    return pr_str(nil);
  }
  
  switch(o->type) {
  case SYMBOL:
    return o->data.s;
        
  case INT:
    int_string = malloc(sizeof(char) * 0xC);
    snprintf(int_string, 0xC, "%d", o->data.i);
    return int_string;
    
  case FLOAT:
    float_string = malloc(sizeof(char) * 0xC);
    snprintf(float_string, 0xC, "%f", o->data.f);
    return float_string;
    
  case LIST:
    list_string = malloc(sizeof(char) * 9999);
    current = o->data.list_head;
    strcat(list_string, "(");
    
    while(current && current->data) {
      strcat(list_string, pr_str(current->data));
      if(current->next->data) {
	strcat(list_string, " ");
      }
      
      current = current->next;
    }
    
    strcat(list_string, ")");
    return list_string;

  case PROC:
    return "#<function//built-in>";

  case NIL:
    return "#nil";

  case TRUE:
    return "#t";

  case FALSE:
    return "#f";
    
  default:
    return "#<non_printable_object>" ;
  }
}



Object *read_lisp(void) {
  char *input = readline(PROMPT);
  
  if(strncmp(input, (const char*)&"exit", 4) == 0) {
    printf("\n");
    exit(0);
  }
  
  add_history(input);
  Reader *r = read_str(input);
  return read_form(r);
}


Object *eval(Object *o, Environment *env) {
  
  if(o->type != LIST) {
    return eval_ast(o, env);
  }

  //Check for empty list
  if(o->type == LIST && (!o->data.list_head || !o->data.list_head->data)) {
    return o;
  }

  if(o->type == LIST) {

    Object *first = car(o);
    Object *new_evald;
    if(first->type == SYMBOL && strcmp(first->data.s, "def!") == 0) {

      Object *s = car(cdr(o));
      Object *v = car(cdr(cdr(o)));
      new_evald = new_Object();
      new_evald->type = LIST;
      new_evald->data.list_head = o->data.list_head;
      
      new_evald->data.list_head->data = first;
      new_evald->data.list_head->next->data = s;
      new_evald->data.list_head->next->next->data = eval_ast(v, env);
      return proc_def(cdr(new_evald));
    } else {
      new_evald = eval_ast(o,env);
    }

    Object *fn_call = car(new_evald);
      
    if(fn_call->type == PROC) {
      new_evald->data.list_head = new_evald->data.list_head->next;
      Object*(*fn)(Object*) = fn_call->data.fn;
      return fn(new_evald);
      
    } else {
      printf("ERORXXXXXX\n");
    }
  }
  return o;
}

Object *eval_ast(Object *o, Environment *env) {
  if(o->type == SYMBOL) {
    return get(o->data.s, env);
  }

  if(o->type == LIST) {
    Object *new_list = calloc(sizeof(Object), 1);
    new_list->type = LIST;
    new_list->data.list_head = new_LLNode();

    LLNode *current_uneval = o->data.list_head;
    LLNode *current_eval = new_list->data.list_head;
    
    while(current_uneval && current_uneval->data) {
      current_eval->data = eval(current_uneval->data, env);

      current_uneval = current_uneval->next;
      current_eval->next = new_LLNode();
      current_eval = current_eval->next;
    }

    return new_list;
        
  }

  return o;
}



Object *proc_add(Object *argp) {
  int total = 0;
  LLNode *current = argp->data.list_head;
  
  while(current) {
    Object *arg = ((Object*)(current->data));
    
    if(arg && arg->type == INT) {
      total += arg->data.i;
    }

    current = current->next;
  }
  
  Object *ret = calloc(sizeof(Object), 1);
  ret->type = INT;
  ret->data.i = total;
  
  return ret;  
}

Object *proc_mul(Object *argp) {
  int total = 1;
  LLNode *current = argp->data.list_head;
  
  while(current) {
    Object *arg = ((Object*)(current->data));
    
    if(arg) {
      total *= arg->data.i;
    }
    
    current = current->next;
  }
  
  Object *ret = new_Object();
  ret->type = INT;
  ret->data.i = total;
  
  return ret;  
}

Object *proc_sub(Object *argp) {
  LLNode *current = argp->data.list_head;
  Object *first = ((Object*)(current->data));
  int total = first->data.i;
  current = current->next;
  
  while(current) {
    Object *arg = ((Object*)(current->data));
    
    if(arg) {
      total -= arg->data.i;
    }
    
    current = current->next;
  }
  
  Object *ret = new_Object();
  ret->type = INT;
  ret->data.i = total;
  
  return ret;  
}

Object *proc_div(Object *argp) {
  LLNode *current = argp->data.list_head;
  Object *first = ((Object*)(current->data));
  int total = first->data.i;
  current = current->next;

  while(current) {
    Object *arg = ((Object*)(current->data));
    
    if(arg) {
      total /= arg->data.i;
    }
    
    current = current->next;
  }
  
  Object *ret = new_Object();
  ret->type = INT;
  ret->data.i = total;
  
  return ret;  
}


Object *proc_def(Object *argp) {

  Object *s = car(argp);
  Object *v = car(cdr(argp));

  set(s->data.s, v, *global_env);
  return v;  
}

void set(char *symbol, void *p, Environment *env) {
  unsigned long hash = djb2(symbol);
  int index = hash & 2047;
  
  env->values[index] = p;
}

Environment *find(char *symbol, Environment *env) {

  while(env) {
    unsigned long hash = djb2(symbol);
    int index = hash & 2047;
    
    if(env->values[index] != NULL) {
      return env;
    }
    
    env = env->outer;   
  }
  
  return NULL;
}

Object *get(char *symbol, Environment *env){
  Environment *e = find(symbol, env);

  if(e != NULL) {
    return env->values[djb2(symbol) & 2047];  
  }
  
  return nil;
}

Environment *init_environment(void) {
  Environment *e = malloc(sizeof(Environment));

  set_proc("+", e, &proc_add);
  set_proc("*", e, &proc_mul);
  set_proc("/", e, &proc_div);
  set_proc("-", e, &proc_sub);
  set_proc("def!", e, &proc_def);

  //#GLOBALS//
  nil = calloc(sizeof(Object), 1);
  nil->type = NIL;
  set("#nil", nil, e);

  true = calloc(sizeof(Object), 1);
  true->type = TRUE;
  set("#t", true, e);

  false = calloc(sizeof(Object), 1);
  false->type = FALSE;
  set("#f", false, e);
  
  return e;
}

void set_proc(char *sym, Environment *e, Object*(fn())) {
  Object *obj = new_Object();
  obj->type = PROC;
  obj->data.fn = fn;
  
  set(sym, obj, e);
}

Object *car(Object *o) {
  if(o->type == LIST) {
    if(o->data.list_head->data) {
      return o->data.list_head->data;

    }
  }

  return nil;
}

Object *cdr(Object *o) {
 if(o->type == LIST) {
    if(o->data.list_head->next->data) {
      Object *new_list = new_Object();
      new_list->type = LIST;
      new_list->data.list_head = o->data.list_head->next;
      return new_list;
    }
  }
 return nil;
}
