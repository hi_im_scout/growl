SRC = repl.c reader.c printer.c common.c

default: program

program: $(SRC)
	gcc $(SRC) -lreadline -g -Wall -pedantic -Wstrict-prototypes -Wmissing-prototypes -Wshadow -Wconversion -o repl
