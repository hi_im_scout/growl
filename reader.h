
#pragma once

#include <string.h>
#include <stdio.h>
#include "common.h"

typedef struct _reader {
  LLNode *current;
} Reader;

Reader *read_str(char *str);

Object *read_form(Reader *r);

Object *read_list(Reader *r);

Object *read_atom(Reader *r);

LLNode *peek(Reader *r);

LLNode *next(Reader *r);

LLNode *tokenize(char *input);


