

#pragma once

#define PROMPT "[lithium] # "
#define BUFSIZE 1000000
#define VERSION "0.7"

typedef struct _node LLNode;
typedef struct _hashmap Environment;
typedef struct _linkedlist LinkedList;
typedef struct _reader Reader;
typedef struct _object Object;
typedef union _objdata ObjData;

typedef struct _node {
  void *data;
  struct _node *next;
} LLNode;

typedef struct _hashmap {
  void *values[2048];
  Environment *outer;
} Environment;

typedef struct _linkedlist {
  LLNode *head;
} LinkedList;

typedef union _objdata {
  int i;
  double f;
  char *s;
  LLNode *list_head;
  Object*(*fn)(Object *argp);
} ObjData;

typedef enum _type {
		    STRING,
		    INT,
		    FLOAT,
		    SYMBOL,
		    LIST,
		    NIL,
		    TRUE,
		    FALSE,
		    LAMBDA,
		    PROC
} Type;

typedef struct _object {
  Type type;
  ObjData data;
} Object;

Object *new_Object(void);

Environment *new_Environment(void);

unsigned long djb2(char *s);

LLNode *new_LLNode(void);
