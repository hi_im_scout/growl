
#include <stdlib.h>
#include "common.h"
#include "reader.h"

extern Object *nil;

LLNode *tokenize(char *s) {
  char *delim = " ";
  char *buffer = calloc(BUFSIZE, 1);
  int buffer_index = 0;
  
  for(int i = 0; i < strlen(s); i++) {
    char c = s[i];
    if(c == '(' || c == ')') {
      buffer[buffer_index++] = ' ';
      buffer[buffer_index++] = c;
      buffer[buffer_index++] = ' ';
    } else {
      buffer[buffer_index++] = c;
    }

  }
  
  buffer[++buffer_index] = '\0';

  LLNode *head = new_LLNode();
  LLNode *current = head;
  char *ptr = strtok(buffer, delim);

  while(ptr) {
    current->data = ptr;
    current->next = new_LLNode();
    current = current->next;
    ptr = strtok(NULL, delim);
  }
  return head;
}

LLNode *peek(Reader *r) {
  return r->current;
}

LLNode *next(Reader *r) {
  LLNode *ret = peek(r);
  if(r->current->next) {
    r->current = r->current->next;
  }
  return ret; 
}

Reader *read_str(char *str) {
  Reader *r = calloc(sizeof(Reader), 1);
  r->current = tokenize(str);
  return r;
}

Object *read_form(Reader *r) {
  char *token = (char *)peek(r)->data;
  char first_char = token[0];
  if(first_char == '(') {
    next(r);
    return read_list(r);
  } else {
    return read_atom(r);
  }
  return nil;
}

Object *read_list(Reader *r) {
  char *token;
  LLNode *head = new_LLNode();
  LLNode *current = head;
  Object *o = new_Object();
  token = (char*)peek(r)->data;
      
  while(token) {
    if(token[0] == ')') {
      next(r);
      break;
    }
    current->data = read_form(r);
    current->next = new_LLNode();
    current = current->next;
    token = (char*)peek(r)->data;
  }

  o->type = LIST;
  o->data.list_head = head;
  return o;
}

Object *read_atom(Reader *r) {
  Type t;
  Object *obj = new_Object();
  char *token = (char *)next(r)->data;
  char first_char = token[0];

  //digit 0-9
  if(first_char >= 0x30 && first_char <= 0x39) {
    //contains decimal point
    if(strchr(token, 0x2E)) {
      t = FLOAT;
    } else {
      t = INT;
    }
  }
  else {
    t = SYMBOL;
  }

  obj->type = t;
  
  switch(t) {
  case SYMBOL:
    obj->data.s = token;
    break;
  case INT:
    obj->data.i = atoi(token);
    break;
  case FLOAT:
    obj->data.f = atof(token);
    break;
  default:
    printf("ERROR!!x invalid character");
    break;
  }
  return obj;
}
