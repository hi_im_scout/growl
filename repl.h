/*

  @author hi_im_scout
*/

#pragma once


void set_proc(char *sym, Environment *e, Object*(fn()));

void set(char *symbol, void *p, Environment *env);

LLNode *new_LLNode(void);

Object *eval(Object *o, Environment *env);

Object *eval_ast(Object *o, Environment *env);

Object *read_lisp(void);

Object *proc_add(Object *argp);// Environment *env);

Object *proc_mul(Object *argp);//, Environment *env);

Object *proc_div(Object *argp);

Object *proc_sub(Object *argp);

Object *proc_def(Object *argp);

Object *get(char *symbol, Environment *env);

Object *car(Object *o);

Object *cdr(Object *o);

Environment *find(char *symbol, Environment *env);

Environment *init_environment(void);

Environment *new_Environment(void);
