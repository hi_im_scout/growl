
#include <stdlib.h>
#include "common.h"

LLNode *new_LLNode(void) {
  LLNode *n = calloc(sizeof(LLNode), 1);
  n->data = NULL;
  n->next = NULL;
  return n;
}

Object *new_Object(void) {
  Object *o = calloc(sizeof(Object), 1);
  return o;
}

unsigned long djb2(char *s) {
  unsigned long hash = 5381;
  int c;

  while((c = *s++)) {
    hash = ((hash << 5) + hash) + (unsigned long)c;
  }

  return hash;
}
